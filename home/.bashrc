# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='\[\e[1;34m\][\u@\h \W]\$\[\e[m\] '
export VISUAL='nvim'
export APP_ANDROID_SDK_PATH='/home/shalva/Android/Sdk/'

