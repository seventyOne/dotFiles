call plug#begin('~/.config/nvim/plugged')
Plug 'rakr/vim-one'
Plug 'jiangmiao/auto-pairs'
Plug 'Valloric/YouCompleteMe'
Plug 'udalov/kotlin-vim'
call plug#end()

set nohlsearch
set ruler number laststatus=2 autoread ttyfast autoindent clipboard=unnamedplus
set history=400 pastetoggle=<F5> so=7 encoding=utf8 ffs=unix,dos,mac
set statusline=%F%m%=[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%r%y%6c,%l/%L\ %P
set list cursorline splitbelow
set listchars=tab:→\ ,eol:¬,trail:⋅,extends:❯,precedes:❮
set timeoutlen=1000 ttimeoutlen=0
set showbreak=↪
set previewheight=5
let mapleader=","
set wildignore=*.o,*.png,*.bmp,*.ico,*.jpg,*.swp,*.tmp,*.gif,*.mp4a
set tabstop=4 shiftwidth=4
set autochdir

noremap s <NOP>
map s :update<Return>
map <Leader>n :tabn<CR>
map <Leader>p :tabp<CR>
map <Leader>N :NERDTree<CR>
nnoremap d "_d
vnoremap d "_d
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
vnoremap < <gv
vnoremap > >gv

set background=light
colorscheme one

let g:deoplete#enable_at_startup = 1
"inoremap <silent><expr> <TAB>
"    \ pumvisible() ? "\<C-n>" :
"    \ <SID>check_back_space() ? "\<TAB>" :
"    \ deoplete#mappings#manual_complete()
"function! s:check_back_space() abort "{{{
"    let col = col('.') - 1
"    return !col || getline('.')[col - 1]  =~ '\s'
"endfunction"}}}

hi PmenuSel ctermfg=251 ctermbg=24
hi Pmenu ctermbg=252 ctermfg=23

let g:ale_fixers = {
\   'javascript': ['eslint'],
\}

function s:MkNonExDir(file, buf)
    if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
        let dir=fnamemodify(a:file, ':h')
        if !isdirectory(dir)
            call mkdir(dir, 'p')
        endif
    endif
endfunction
augroup BWCCreateDir
    autocmd!
    autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END
    let &t_ti.="\<Esc>[1 q"
    let &t_SI.="\<Esc>[5 q"
    let &t_EI.="\<Esc>[1 q"
    let &t_te.="\<Esc>[0 q"

