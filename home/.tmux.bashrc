# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias i3config='nvim ~/.config/i3/config'
alias v='nvim'
alias r='ranger'
alias p='python'
alias se='sudoedit'
alias ..='cd ..'
alias start='sudo systemctl start'
alias status='sudo systemctl status'
alias stop='sudo systemctl stop'
alias restart='sudo systemctl restart'
alias deepin-screenshot='deepin-screenshot -n'
alias s='sudo'
alias Node='node ~/Projects/Scripts/nodeTest.js'
alias TV='sudo teamviewer --daemon start && teamviewer'
alias t='top'
alias k='kotlinc -script'
PS1='\[\e[1;34m\]>>\[\e[m\] '

#export http_proxy='192.168.0.100:8888'
#export https_proxy='192.168.0.100:8888'
#export ftp_proxy=''
#export socks_proxy=''
#export WINEARCH=win32

noderepl() {
    FILE_CONTENTS="$(< $1 )"
    node -i -e "$FILE_CONTENTS"
}
